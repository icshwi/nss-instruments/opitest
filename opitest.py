import re, collections
import sys
from lxml import etree
  


###############################################################################
def checkWidgetLabel(w):
   retval = 0
   line = w.sourceline

   name = w.find('.//name')
   clas = w.find('.//class')
   text = w.find('.//text')
   x = w.find('.//x')
   y = w.find('.//y')
   width = w.find('.//width')
   height = w.find('.//height')
   font = w.find('.//font/font')
   fcolor = w.find('.//foreground_color/color')

   if name is not None:
      nameS = name 
   if clas is not None:
      clasS = clas 
   if text is not None:
      textS = text 
   if x is not None:
      xS = x 
   if y is not None:
      yS = y 
   if width is not None:
      widthS = width 
   if height is not None:
      heightS = height 
   if font is not None:
      if font.attrib['family'] != 'Source Sans Pro':
          print('WARNING: {} ({}):'.format(filename, line))
          print('\tWIDGET TYPE: label, name: {}, text: {}'.format(nameS.text,textS.text))
          print('\tfont family expected [Source Sans Pro] found', font.attrib['family']) 
          retval = retval + 1
      if font.attrib['style'] != 'BOLD':
          print('WARNING: {} ({}):'.format(filename, line))
          print('\tWIDGET TYPE: label, name: {}, text: {}'.format(nameS.text,textS.text))
          print('\tfont family expected [BOLD] found', font.attrib['style']) 
          retval = retval + 1
      if font.attrib['size'] != '16.0':
          print('WARNING: {} ({}):'.format(filename, line))
          print('\tWIDGET TYPE: label, name: {}, text: {}'.format(nameS.text,textS.text))
          print('\tfont family expected [16.0] found', font.attrib['size']) 
          retval = retval + 1
   #if fcolor is not None:
   #   fcolorS = fcolor 

   return retval

###############################################################################
def checkWidgetTextentry(w):
   retval = 0
   line = w.sourceline

   name = w.find('.//name')
   #clas = w.find('.//class')
   #text = w.find('.//text')
   x = w.find('.//x')
   y = w.find('.//y')
   width = w.find('.//width')
   height = w.find('.//height')
   font = w.find('.//font/font')
   foreground_color = w.find('.//foreground_color/color')
   background_color = w.find('.//background_color/color')
   border_width = w.find('.//border_width')
   border_color = w.find('.//border_color/color')

   if name is not None:
      nameS = name 
   #if clas is not None:
   #   clasS = clas 
   #if text is not None:
   #   textS = text 
   if x is not None:
      xS = x 
   if y is not None:
      yS = y 
   if width is not None:
      widthS = width 
   if height is not None:
      heightS = height 
   if font is not None:
      if font.attrib['family'] != 'Source Sans Pro':
          print('WARNING: {} ({}):'.format(filename, line))
          print('\tWIDGET TYPE: textentry, name: {}'.format(nameS.text))
          print('\tExpected font family [Source Sans Pro]')
          print('\tFound [{}]'.format(font.attrib['family'])) 
          retval = retval + 1
      if font.attrib['style'] != 'REGULAR':
          print('WARNING: {} ({}):'.format(filename, line))
          print('\tWIDGET TYPE: textentry, name: {}'.format(nameS.text))
          print('\tExpected font style [BOLD]')
          print('\tFound', font.attrib['style']) 
          retval = retval + 1
      if font.attrib['size'] != '16.0':
          print('WARNING: {} ({}):'.format(filename, line))
          print('\tWIDGET TYPE: textentry, name: {}'.format(nameS.text))
          print('\tExpected font size: [16.0]')
          print('\tFound:', font.attrib['size']) 
          retval = retval + 1
   if foreground_color is not None:
      if foreground_color.attrib['name'] != 'Text':
          print('WARNING: {} ({}):'.format(filename, line))
          print('\tWIDGET TYPE: textentry, name: {}'.format(nameS.text))
          print('\tExpected defined foreground_color: [Text], RGB(25,25,25)') 
          print('\tFound: [{}], RGB({},{},{})'.format(foreground_color.attrib['name'], 
              foreground_color.attrib['red'], foreground_color.attrib['green'], 
              foreground_color.attrib['blue']))
          retval = retval + 1
   if background_color is not None:
      if background_color.attrib['name'] != 'Text':
          print('WARNING: {} ({}):'.format(filename, line))
          print('\tWIDGET TYPE: textentry, name: {}'.format(nameS.text))
          print('\tExpected defined background_color: [Write_Background], RGB(230,235,232)') 
          print('\tFound: [{}], RGB({},{},{})'.format(background_color.attrib['name'], 
              background_color.attrib['red'], background_color.attrib['green'], 
              background_color.attrib['blue']))
          retval = retval + 1
    
   if border_width is not None:
      if border_width.text != '0': 
          print('WARNING: {} ({}):'.format(filename, line))
          print('\tWIDGET TYPE: textentry, name: {}'.format(nameS.text))
          print('\tExperted [border_width] equal 0')
          print('\tFound: border_width =',border_width.text) 
          retval = retval + 1

   if border_color is not None:
      if border_color.attrib['name'] != 'Text':
          print('WARNING: {} ({}):'.format(filename, line))
          print('\tWIDGET TYPE: textentry, name: {}'.format(nameS.text))
          print('\tExpected defined border_color: [Text], RGB(25,25,25)') 
          print('\tFound: [{}], RGB({},{},{})'.format(border_color.attrib['name'], 
              border_color.attrib['red'], border_color.attrib['green'], 
              border_color.attrib['blue']))
          retval = retval + 1
    
   return retval



###############################################################################
def checkWidgetTextupdate(w):
   retval = 0
   line = w.sourceline

   name = w.find('.//name')
   #clas = w.find('.//class')
   #text = w.find('.//text')
   x = w.find('.//x')
   y = w.find('.//y')
   width = w.find('.//width')
   height = w.find('.//height')
   font = w.find('.//font/font')
   foreground_color = w.find('.//foreground_color/color')
   background_color = w.find('.//background_color/color')
   border_width = w.find('.//border_width')
   border_color = w.find('.//border_color/color')

   if name is not None:
      nameS = name 
   #if clas is not None:
   #   clasS = clas 
   #if text is not None:
   #   textS = text 
   if x is not None:
      xS = x 
   if y is not None:
      yS = y 
   if width is not None:
      widthS = width 
   if height is not None:
      heightS = height 
   if font is not None:
      if font.attrib['family'] != 'Source Sans Pro':
          print('WARNING: {} ({}):'.format(filename, line))
          print('\tWIDGET TYPE: textupdate, name: {}'.format(nameS.text))
          print('\tExpected font family [Source Sans Pro]')
          print('\tFound [{}]'.format(font.attrib['family'])) 
          retval = retval + 1
      if font.attrib['style'] != 'REGULAR':
          print('WARNING: {} ({}):'.format(filename, line))
          print('\tWIDGET TYPE: textupdate, name: {}'.format(nameS.text))
          print('\tExpected font style [BOLD]')
          print('\tFound', font.attrib['style']) 
          retval = retval + 1
      if font.attrib['size'] != '16.0':
          print('WARNING: {} ({}):'.format(filename, line))
          print('\tWIDGET TYPE: textupdate, name: {}'.format(nameS.text))
          print('\tExpected font size: [16.0]')
          print('\tFound:', font.attrib['size']) 
          retval = retval + 1
   if foreground_color is not None:
      if foreground_color.attrib['name'] != 'Text':
          print('WARNING: {} ({}):'.format(filename, line))
          print('\tWIDGET TYPE: textupdate, name: {}'.format(nameS.text))
          print('\tExpected defined foreground_color: [Text], RGB(25,25,25)') 
          print('\tFound: [{}], RGB({},{},{})'.format(foreground_color.attrib['name'], 
              foreground_color.attrib['red'], foreground_color.attrib['green'], 
              foreground_color.attrib['blue']))
          retval = retval + 1
   if background_color is not None:
      if background_color.attrib['name'] != 'Text':
          print('WARNING: {} ({}):'.format(filename, line))
          print('\tWIDGET TYPE: textupdate, name: {}'.format(nameS.text))
          print('\tExpected defined background_color: [Read_Background], RGB(230,235,232)') 
          print('\tFound: [{}], RGB({},{},{})'.format(background_color.attrib['name'], 
              background_color.attrib['red'], background_color.attrib['green'], 
              background_color.attrib['blue']))
          retval = retval + 1
    
   if border_width is not None:
      if border_width.text != '0': 
          print('WARNING: {} ({}):'.format(filename, line))
          print('\tWIDGET TYPE: textupdate, name: {}'.format(nameS.text))
          print('\tExperted [border_width] equal 0')
          print('\tFound: border_width =',border_width.text) 
          retval = retval + 1

   if border_color is not None:
      if border_color.attrib['name'] != 'Text':
          print('WARNING: {} ({}):'.format(filename, line))
          print('\tWIDGET TYPE: textupdate, name: {}'.format(nameS.text))
          print('\tExpected defined border_color: [Text], RGB(25,25,25)') 
          print('\tFound: [{}], RGB({},{},{})'.format(border_color.attrib['name'], 
              border_color.attrib['red'], border_color.attrib['green'], 
              border_color.attrib['blue']))
          retval = retval + 1
    
   return retval

###############################################################################
def checkWidget(w):
    widgetType = widget.attrib['type']

    if   widgetType == 'label':
       return checkWidgetLabel(w)

    elif widgetType == 'textupdate':
       return checkWidgetTextupdate(w)

    elif widgetType == 'textentry':
       return checkWidgetTextentry(w)

    else:
        print('WARNING: {} ({})'.format(filename, w.sourceline))
        print('\tWidget {} not supported yet'.format(widgetType))
        return -1

###############################################################################
###############################################################################
###############################################################################
#                                                                             #
#    Start script                                                             #
#                                                                             #
###############################################################################


filename = 'test.bob'
#root = etree.parse(sys.argv[1]).getroot()
root = etree.parse(filename).getroot()

loops = 0
warnings = 0
nsupport = 0

for widget in root.iter('widget'):
    nr = checkWidget(widget)
    if nr > 0:
        warnings = warnings + nr
    else:
        nsupport = nsupport + abs(nr)

    loops = loops + 1

print('\n')
print('########################################')
print('# Test results:')
print('########################################')
print('#   file:',filename)
print('#       - number of widgets' , loops) 
print('#       - number of warnings', warnings)
print('#       - not supported', nsupport)
print('########################################')


