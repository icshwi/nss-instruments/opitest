import re, collections
import sys
from lxml import etree
  

#xml_root = etree.parse(sys.argv[1]).getroot()
xml_root = etree.parse('kepco.bob').getroot()
raw_tree = etree.ElementTree(xml_root)
nice_tree = collections.OrderedDict()


for tag in xml_root.iter():
    path = re.sub('\[[0-9]+\]', '', raw_tree.getpath(tag))
#    print('path', path.split('/')[-1])
    #print('tag.keys',tag.keys(1))
    if path not in nice_tree:
        nice_tree[path] = []
    if len(tag.keys()) > 0:
 #       for atrib in tag.keys():
 #           print('atrib',atrib)
        nice_tree[path].extend(attrib for attrib in tag.keys() if attrib not in nice_tree[path])            

 
for path, attribs in nice_tree.items():
    indent = int(path.count('/') - 1)
    #print('{0}{1}: {2} [{3}]'.format('    ' * indent, indent, path.split('/')[-1], ', '.join(attribs) if len(attribs) > 0 else attribs.get())
    print('{0}{1}: {2} [{3}]'.format('    ' * indent, indent, path.split('/')[-1], ', '.join(attribs) if len(attribs) > 0 else '-'))
