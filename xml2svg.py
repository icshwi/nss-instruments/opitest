import xml.etree.ElementTree
import graphviz

tree = xml.etree.ElementTree.parse('example.xml')
root = tree.getroot()

# read all the structures
for structure in root.findall('.//Structures/Structure'):
    structure_name = structure.findtext('./Name')
    graph = graphviz.Digraph(format='svg',comment='structure_name')

    # read the nodes of the structure
    for node in structure.findall('.//Nodes/Node'):
        node_element_id = node.findtext('./ElementID')
        # find the name
        element_name = root.findtext(".//Elements/Element[ID='{}']/Name".format(node_element_id))
        graph.node(node_element_id, element_name)

    # read the links
    for link in structure.findall('.//Links/Link'):
        id_1 = link.findtext('./LHS')
        id_2 = link.findtext('./RHS')
        description = link.findtext('./Description')
        direction = link.findtext('./Direction')
        graph.edge(id_1, id_2, label=description)
        if direction=='Double':
            graph.edge(id_2, id_1, label=description)

    graph.render(filename=structure_name)
